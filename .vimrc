" Always show statusline
set laststatus=2

" Use 256 colours (Use this setting only if your terminal supports 256 colours)
set t_Co=256

:filetype on
:au FileType c,cpp,java set cindent

filetype plugin indent on

set nocompatible
set tabstop=4
set shiftwidth=4
" On pressing tab, insert 4 spaces
set expandtab
set showmode
set textwidth=80
set pastetoggle=<f2>
set ai
"set backspace=2
if &t_Co > 1
	syntax enable
endif

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => VIM user interface
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Set 7 lines to the curors - when moving vertical..
set so=7

set wildmenu "Turn on WiLd menu

set number "Show line numbers
set ruler "Always show current position

set cmdheight=2 "The commandbar height

set hid "Change buffer - without saving

" Set backspace config
set backspace=eol,start,indent
set whichwrap+=<,>,h,l

set ignorecase "Ignore case when searching
set smartcase

set hlsearch "Highlight search things

set incsearch "Make search act like search in modern browsers
set nolazyredraw "Don't redraw while executing macros 

set magic "Set magic on, for regular expressions

set showmatch "Show matching bracets when text indicator is over them
set mat=2 "How many tenths of a second to blink

" No sound on errors
set noerrorbells
set novisualbell
set t_vb=
set tm=500

""""""""""""""""""""""""""""""
" => Colors and Fonts
""""""""""""""""""""""""""""""
syntax enable "Enable syntax hl
" Set font according to system
if has("gui_running")
  set guioptions-=T
  set t_Co=256
  set background=dark
  colorscheme peaksea
  set nonu
else
  colorscheme zellner
  set background=dark

  set nonu
endif

""""""""""""""""""""""""""""""
" => Macros [map {10 char} {100 char}]
""""""""""""""""""""""""""""""
map <lhs> <rhs>

""""""""""""""""""""""""""""""
" => Abbreviations
""""""""""""""""""""""""""""""
ab func function

