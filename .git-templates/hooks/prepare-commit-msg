#!/bin/sh

# A hook script to prepare the commit log message.
# Called by "git commit" with the name of the file that has the
# commit message, followed by the description of the commit
# message's source. The hook's purpose is to edit the commit
# message file. If the hook fails with a non-zero status,
# the commit is aborted.
#
# To enable this hook, this file must be named "prepare-commit-msg".

# If the current branch has a issue number in the name,
# e.g. ISSUE-404, this will append the issue number to the
# bottom of the commit message so that it is placed in the
# commit body/description.
#
# "$2" is the source of the commit message and can be:
#   - message (if a -m or -F option was given)
#   - template (if a -t option was given or the configuration option
#   commit.template is set)
#   - merge (if the commit is a merge or a .git/MERGE_MSG file exists)
#   - squash (if a .git/SQUASH_MSG file exists)
#   - commit, followed by a commit SHA-1 (if a -c, -C, or --amend option was
#   given)
# A check is done to see if "$2" is set and does nothing if it is because they
# are situations where one usually does not want the issue number.
if [ x = x${2} ]; then
    FILE=$1
    MESSAGE=$(cat $FILE)
    BRANCH_NAME=$(git symbolic-ref --short HEAD)
    ISSUE=$(echo $BRANCH_NAME | grep -Eo '^(\w+/)?(\w+[-_])?\d+' | grep -Eo '(\w+[-])?\d+' | tr "[:lower:]" "[:upper:]")

    # Ensure that the issue was parsed and is not part of the message.
    if [ -n $ISSUE ] && [ x != x${ISSUE} ]; then
        echo "\n\n${ISSUE} ${MESSAGE}" > $FILE
    fi
fi

