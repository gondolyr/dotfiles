#!/usr/bin/env sh

# Copyright (C) 2024  gondolyr
# SPDX-FileCopyrightText: 2024 gondolyr <gondolyr+code@posteo.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# Download the latest video from a Twitch channel, a specific video, or clip.

: ${VIDEO_DIR:="/run/media/$USER/data/videos"}
: ${TEMP_DIR:="/tmp"}

usage() {
    cat <<EOF

$0 [-s <start>] [-e <end>] [-f <format>] [-q <quality>] [-c <channel>] [-v <video>]

DESCRIPTION

    Download the latest video from a Twitch channel, a specific video, or clip.

    twitch-dl must be installed.
    See <https://github.com/ihabunek/twitch-dl> for installation instructions.

OPTIONS

    -c <channel>
        Download the latest video from this channel.

    -e <end>
        Download the video up to this time (hh:mm or hh:mm:ss).

    -f <format>
        Video format to convert into, passed to ffmpeg as the target file extension (default: mkv)

    -q <quality>
        Video quality, e.g. 720p. Set to 'source' to get best quality.

    -s <start>
        Download the video from this time (hh:mm or hh:mm:ss).

    -v <video>
        Download the video from this URL, video ID, or clip slug.

        This takes priority over other identifier options such as `-c`.
EOF
}

# Fetch information for a given Twitch URL, video ID, or clip slug.
get_video_info() {
    local video="$1"

    twitch-dl info "${video}"
}

# Fetch the latest video info for a given channel.
get_latest_channel_video() {
    local channel="$1"

    twitch-dl videos -l 1 "${channel}"
}

# Download a Twitch video to the given directory.
download_vod_to_dir() {
    local start="$1"
    local end="$2"
    local format="$3"
    if [ -z "${format}" ]; then
        format="mkv"
    fi
    local quality="$4"
    if [ -z "${quality}" ]; then
        quality="source"
    fi
    local id="$5"
    local output="$6"
    local channel="$7"
    local normalized_channel=$(echo "${channel}" | tr "[[:upper:]]" "[[:lower:]]")

    if [ ! -d "${output}" ]; then
        mkdir -p "${output}"
    fi

    (
        cd "${TEMP_DIR}"

        local start_option="${start}"
        if [ $start_option ]; then
            start_option="-s ${start_option}"
        fi
        local end_option="${end}"
        if [ $end_option ]; then
            end_option="-e ${end_option}"
        fi

        twitch-dl download \
            -q source \
            ${start_option} \
            ${end_option} \
            -f ${format} \
            -q ${quality} \
            ${id}

        start=$(echo "${start}" | sed -e "s/://g")
        end=$(echo "${end}" | sed -e "s/://g")

        local original_filename=$(
            ls "${TEMP_DIR}" \
                | grep -i "${id}_${channel}"
        )

        if [ -z $original_filename ]; then
            echo "there was a problem downloading the video"
            exit 1
        fi

        local new_filename=$(
            echo "${channel}_${original_filename}" \
                | sed -e "s/${id}_${normalized_channel}/${id}_${start}-${end}_${normalized_channel}/g"
        )

        echo "moving download to ${output}/${new_filename}"
        mv "${TEMP_DIR%%/}/${original_filename}" "${output}/${new_filename}"
    )
}

# Remove terminal color decorations from the given string.
#
# The input may be provided or piped in.
_strip_colors() {
    local input

    if [ $# -eq 0 ]; then
        input=$(cat <&0)
    else
        input="$1"
    fi

    echo "${input}" \
        | sed -E "s/\x1B\[([[:digit:]]{1,2}(;[[:digit:]]{1,2})?)?[mGK]//g"
}

if [ ! -x $(which twitch-dl) ]; then
    echo "unable to find twitch-dl installation" >&2
    exit 1
fi

if [ $# -eq 0 ]; then
    usage >&2
    exit 0
fi

start_time="00:00:00"
while getopts "c:e:f:q:hs:v:" opt; do
    case ${opt} in
        c)
            channel="${OPTARG}"
            ;;
        e)
            end_time="${OPTARG}"
            if [ ${#end_time} -lt 4 ]; then
                # Input is "xx", "h:mm", or "hh:m"
                echo "ambiguous end time ${end_time}"
                usage >&2
                exit 1
            elif [ ${#end_time} -eq 4 ]; then
                # Time is "h:mm".
                end_time="0${end_time}:00"
            elif [ ${#end_time} -eq 5 ]; then
                # Time is "hh:mm".
                end_time="${end_time}:00"
            fi
            ;;
        f)
            format="${OPTARG}"
            ;;
        h)
            usage
            exit 0
            ;;
        q)
            quality="${OPTARG}"
            ;;
        s)
            start_time="${OPTARG}"
            if [ ${#start_time} -lt 4 ]; then
                # Input is "xx", "h:mm", or "hh:m"
                echo "ambiguous start time ${start_time}"
                usage >&2
                exit 1
            elif [ ${#start_time} -eq 4 ]; then
                # Time is "h:mm".
                start_time="0${start_time}:00"
            elif [ ${#start_time} -eq 5 ]; then
                # Time is "hh:mm".
                start_time="${start_time}:00"
            fi
            ;;
        v)
            video="${OPTARG}"
            ;;
        \?)
            echo "unexpected option: ${OPTARG}" >&2
            usage >&2
            exit 1
            ;;
    esac
done
shift $((OPTIND - 1))

if [ -z "${channel}" ] && [ -z "${video}" ]; then
    echo "no channel or video identifier provided"
    usage >&2
    exit 0
fi

if [ "${video}" ]; then
    video_info=$(get_video_info "${video}")
else
    video_info=$(get_latest_channel_video "${channel}")
fi

if [ -z "${video_info}" ] \
    || [ $(echo "${video_info}" | grep -o "No videos found") ]; then
    echo "no video found"
    exit 1
fi

id=$(
    echo "${video_info}" \
        | grep -sE "Video [[:digit:]]+" \
        | cut -d ' ' -f 2
)
normalized_channel_name=$(
    echo "${video_info}" \
        | grep -isE "${channel} playing " \
        | cut -d ' ' -f 1
)

# "2 h 43" - We don't care about the minutes unit in this example.
length=$(
    echo "$video_info" \
        | grep -Eo "Length: [0-9A-Za-z ]+" \
        | cut -d ' ' -f 2,3,4
)

hours=$(echo "${end_time}" | cut -d ':' -f 1)
minutes=$(echo "${end_time}" | cut -d ':' -f 2)
seconds=$(echo "${end_time}" | cut -d ':' -f 3)

# Boolean tracking if the end time input has been modified in the normalization.
# If so, another minute needs to be added to capture the entire VoD.
end_time_rounded=0

# Normalize the end time.
if [ $(echo $length | cut -d ' ' -f 2) = "h" ]; then
    length_hours=$(echo "$length" | cut -d ' ' -f 1)
    if [ -z "${length_hours}" ]; then
        length_hours=0
    fi
    length_minutes=$(echo "$length" | cut -d ' ' -f 3)
    if [ -z "${length_minutes}" ]; then
        length_minutes=0
    fi

    # Set the end time to the video length if:
    #     - There is no end time set.
    #     - The input hours from the given end time is greater than the video length.
    #     - The input hours from the given end time is greater than or equal to the video length and the input minutes exceeds the video length.
    if [ -z "${end_time}" ] \
        || [ "${hours}" -gt "${length_hours}" ] \
        || { [ "${hours}" -ge "${length_hours}" ] \
            && [ "${minutes}" -gt "${length_minutes}" ]; }; then
        hours=$length_hours
        minutes=$length_minutes
        end_time_rounded=1
    fi
else
    hours=0
    length_minutes=$(echo $length | cut -d ' ' -f 1)
    if [ -z "${end_time}" ] \
        || [ "${minutes}" -gt "${length_minutes}" ]; then
        minutes=$length_minutes
        end_time_rounded=1
    fi
fi

# Since twitch-dl doesn't return the seconds information,
# add 1 to the minutes to ensure the download captures the remaining video.
if [ "${end_time_rounded}" -eq 1 ]; then
    minutes=$(($minutes + 1))
    seconds="00"
    if [ $minutes -eq 60 ]; then
        hours=$(($hours + 1))
        minutes=0
    fi
fi

if [ -z "${seconds}" ]; then
    seconds="00"
fi

# Trim prefixed 0s because they are treated as hexadecimal when formatting.
end_time=$(printf "%02d:%02d:%02d" "${hours##0}" "${minutes##0}" "${seconds##0}")

output_dir="${VIDEO_DIR%%/}/${normalized_channel_name}"
echo "saving ${id} to ${output_dir}"
download_vod_to_dir \
    "${start_time}" \
    "${end_time}" \
    "${format}" \
    "${quality}" \
    "${id}" \
    "${output_dir}" \
    "${normalized_channel_name}"
