#!/usr/bin/env sh
#
# Clone all of a user's repositories.

: ${PROJECTS_DIR:="."}

: ${GITLAB_BASE_URL:="https://gitlab.com/api/v4"}
: ${GITLAB_USER:=""}
# See: https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#creating-a-personal-access-token
: ${PRIVATE_TOKEN:=""}
: ${MAX_RESULTS_PER_PAGE:="100"}

# Send an HTTP GET request to GitLab.
_gitlab_get() {
    local path=$1

    curl -H "PRIVATE-TOKEN: ${PRIVATE_TOKEN}" "${GITLAB_BASE_URL}/${path}"
}

# Get a list of the given user's projects.
_gitlab_get_user_projects() {
    local user=$1

    _gitlab_get "users/${user}/projects/?page=1&per_page=${MAX_RESULTS_PER_PAGE}"
}

(
    echo "Cloning into '${PROJECTS_DIR}'\n"
    cd "${PROJECTS_DIR}"

    for repo in $(
        _gitlab_get_user_projects "${GITLAB_USER}" \
            | jq .[].ssh_url_to_repo \
            | tr -d '"'
    ); do
        git clone $repo
        echo ""
    done
)
