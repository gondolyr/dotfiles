#!/usr/bin/env sh

# Compress a directory's subfolders into `.cbz` archives.
#
# WARNING: This will remove the original files!

usage() {
    cat <<EOF
$0 [-d] <comicspath>

DESCRIPTION

    Compress a directory's subfolders into `.cbz` archives.

    To use this, you must be in a Git repository.

ARGS

    <comicspath>
        Tell the program where to convert chapter directories into .cbz archives.

        For example, "/home/myuser/Documents/My Comics"

OPTIONS

    -d
        Delete the original chapter directories.

        For example, if a chapter directory is located at "/home/myuser/Documents/My Comics/Comic 1/Vol.1 Ch.1 - A New Beginning", a .cbz file will be created at "/home/myuser/My Comics/Comic 1/Vol.1 Ch.1 - A New Beginning.cbz" and the directory "/home/myuser/Documents/My Comics/Comic 1/Vol.1 Ch.1 - A New Beginning" will be removed.

The expected directory structure of the passed path is as follows:

.
└── My Comics/
    ├── Comic 1/
    │   ├── Vol.1 Ch.1 - The Beginning/
    │   │   ├── 001.png
    │   │   ├── 002.png
    │   │   └── 003.png
    │   └── Vol.1 Ch.2 - A Moment's Respite/
    │       ├── 001.jpg
    │       └── 002.jpg
    └── Comic 2/
        ├── Vol.3 Ch.1 - Country Life/
        │   ├── 001.jpg
        │   ├── 002.jpg
        │   ├── 003.jpg
        │   └── 004.jpg
        └── Vol.3 Ch.2 - Sunset/
            ├── 001.png
            └── 002.png
EOF
}

delete_original=0
while getopts "d" opt; do
    case ${opt} in
        d)
            delete_original=1
            ;;
        \?)
            echo "unexpected option: ${OPTARG}" >&2
            echo "usage: " >&2
            usage >&2
            exit 1
            ;;
    esac
done
shift $((OPTIND - 1))

# Example: "/home/myuser/Documents/My Comics".
root_dir="${1%%/}"

if [ ! -d "${root_dir}" ]; then
    echo "'${root_dir}' is not a valid directory" >&2
    usage >&2
    exit 1
fi

# `title_path` example: "/home/myuser/Documents/My Comics/Comic 1".
for title_path in "${root_dir}"/*; do
    # `chapter_path` example: "/home/myuser/Documents/My Comics/Comic 1/Vol.1 Ch.1 - The Beginning".
    for chapter_path in "${title_path}"/*; do
        if [ ! -d "${chapter_path}" ]; then
            continue
        fi

        # Flag explanation:
        #   -r: Recursively archive the files.
        #   -D: Do not create directories.
        #
        # Example:
        # This creates a .cbz file at:
        #   "/home/myuser/Documents/My Comics/Comic 1/ Vol.1 Ch.1 - The Beginning.cbz"
        zip -r -D "${chapter_path}.cbz" "${chapter_path}"

        if [ $delete_original -eq 1 ]; then
            rm -rf "${chapter_path}"
        fi
    done
done
