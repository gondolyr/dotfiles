#!/usr/bin/env sh

# Utility script to create an mpv config file based on the currently installed version's options.
#
# For full descriptions of each option, refer to the `man` page or see https://mpv.io/manual/master/.
#
# Refer to the "CONFIGURATION FILES" section of the `man` page or see https://mpv.io/manual/master/#configuration-files for instructions on how to install the configuration file.
#
# This script will output to stdout so redirect it if you wish to save it to a file; see Usage for more information.
#
# Configuration descriptions will have a space between the hash and the string while options will not.
# Sub-options are prefixed with a hash but have more than one space between the hash and option.
#
# For example:
#
#   # This is a description.
#   #option-key="default value"
#   #   suboption
#
# Usage
#
# Save the configuration to `mpv.conf` in the current working directory.
#
# $0 > mpv.conf

config_header() {
    mpv_version="$(mpv --version | grep -Eo "mpv [[:digit:]]+\.[[:digit:]]+\.[[:digit:]]+")"

    cat <<EOF
# Configuration for mpv, a media player based on MPlayer and mplayer2.
#
# ${mpv_version}
#
# For full descriptions of each option, refer to the `man` page or see https://mpv.io/manual/master/.
#
# Refer to the "CONFIGURATION FILES" section of the `man` page or see https://mpv.io/manual/master/#configuration-files for instructions on how to install the configuration file.
#
# Configuration descriptions will have a space between the hash and the string while options will not.
# Sub-options are prefixed with a hash but have more than one space between the hash and option.
#
# For example:
#
#   # This is a description.
#   #option-key="default value"
#   #   suboption
EOF
}

# Relatively logical sections from the available options.
# Each value is an mpv option.
SECTIONS="
    access-references
    ad
    af
    aid
    alang
    alpha
    alsa-buffer-time
    ao
    aspect
    ass
    audio
    autofit
    autoload-files
    autosub
    autosync
    background
    benchmark
    blend-subtitles
    bluray-angle
    border
    brightness
    cache
    capture
    cdda-cdtext
    cdrom-device
    channels
    chapter
    config
    contrast
    cookies
    correct-downscaling
    cover-art-auto
    cscale
    cursor-autohide
    deband
    deinterlace
    delay
    demuxer
    display-fps
    dither
    drm-atomic
    dscale
    dump-stats
    dvbin-card
    dvd-angle
    edition
    embeddedfonts
    end
    error-diffusion
    external-file
    fbo-format
    fit-border
    focus-on-open
    force-media-title
    fps
    framedrop
    fs
    gamma
    gamut-clipping
    gapless-audio
    geometry
    glsl-shader
    gpu-api
    hdr-compute-peak
    hidpi-window-scale
    hls-bitrate
    hr-seek
    http-header-fields
    hue
    hwdec
    icc-3dlut-size
    idle
    ignore-path-in-watch-later-config
    image-display-duration
    include
    index
    intitial-audio-sync
    input-ar-delay
    interpolation
    jack-autostart
    keep-open
    keepaspect
    lavfi-complex
    length
    linear-downscaling
    load-auto-profiles
    log-file
    loop
    mc
    merge-files
    metadata-codepage
    mf-fps
    monitoraspect
    msg-color
    mute
    native-fs
    network-timeout
    oac
    ocopy-metadata
    of
    on-all-workspaces
    ontop
    opengl-check-pattern-a
    orawts
    ordered-chapters
    oremove-metadata
    osc
    osd-align-x
    oset-metadata
    ovc
    override-display-fps
    panscan
    pause
    play-dir
    player-operation-mode
    playlist
    profile
    pulse-allow-suspended
    quiet
    rar-list-all-volumes
    really-quiet
    rebase-start-time
    referrer
    replaygain
    reset-on-next-file
    resume-playback
    rtsp-transport
    saturation
    save-position-on-quit
    scale
    screen
    screenshot-directory
    script
    sdl-buflen
    secondary-sid
    sharpen
    show-profile
    shuffle
    sid
    sigmoid-center
    slang
    snap-window
    speed
    spirv-compiler
    sstep
    start
    stop-playback-on-init-failure
    stop-screensaver
    stream-buffer-size
    stretch-dvd-subs
    subswapchain-depth
    sws-allow-simg
    target-peak
    taskbar-progress
    teletext-page
    temporal-dither
    term-osd
    terminal
    title
    tls-ca-file
    tone-mapping
    track-auto-selection
    tscale
    untimed
    use-embedded-ics-profile
    use-filedir-conf
    user-agent
    vaapi-device
    vd
    vf
    vid
    vlang
    vo
    volume
    vulkan-async-compute
    watch-later-directory
    wayland-app-id
    wid
    write-filename-in-watch-later-config
    x11-bypass-compositor
    xv-adaptor
    ytdl
    zimg-dither
"

if [ ! -x $(which mpv) ]; then
    echo "unable to find mpv installation" >&2
    exit 1
fi

mpv_raw_options=$(mpv --list-options)

echo "$(config_header)\n"

# Output the configuration options.
echo "${mpv_raw_options}" | while read -r line; do
    # Skip lines that are not valid options.
    if [ -z "${line}" ] \
        || [ "$(echo "${line}" | grep "Options:")" ] \
        || [ "$(echo "${line}" | grep "Total:")" ]; then
        continue
    fi

    # Field separation.

    opt=$(echo "${line}" | cut -d " " -f 1)
    # The quotes are intentionally left out when echo-ing to remove extraneous whitespace.
    description=$(echo ${line} | cut -d " " -f 2-)
    if [ "${description}" = "${opt}" ]; then
        description=""
    fi
    default_value=$(
        echo "${description}" \
            | grep -Eo "default: \w+" \
            | cut -d " " -f 2
    )

    # Skip options that are deprecated or not available for the configuration file.
    if [ "${description}" != "${description##*\[deprecated\]*}" ] \
        || [ "${description}" != "${description##*\[not in config files\]*}" ]; then
        continue
    fi

    # Output formatting.

    # Strip the prefixing double dashes because the configuration file doesn't use them.
    opt="${opt##--}"

    if [ "${description}" ]; then
        description="# ${description}\n"
    else
        # This is a sub-option.
        opt="   ${opt}"
    fi

    if [ "${default_value}" ]; then
        opt_and_default="${opt}=\"${default_value}\""
    else
        opt_and_default="${opt}"
    fi

    section_break=""
    for section in $SECTIONS; do
        if [ "${opt}" = "${section}" ]; then
            section_break="\n"
            break
        fi
    done

    echo "${section_break}${description}#${opt_and_default}"
done
