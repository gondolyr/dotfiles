#!/usr/bin/env sh

# Copyright (C) 2024  gondolyr
# SPDX-FileCopyrightText: 2024 gondolyr <gondolyr+code@posteo.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# dependencies:
#  base: curl

# Use TERM to exit on error.
trap "exit 1" TERM
export TOP_PID=$$

# Dependencies
DEPS_BASE="curl"

check_root() {
    if [ ! "${EUID:-$(id -u)}" -eq 0 ]; then
        die "This script must be run as root; re-run prefixed with sudo"
    fi
}

die() {
    local msg=$1

    if [ ! -z "$msg" ]; then
        echo "\n$msg\n"
    fi

    kill -s TERM $TOP_PID

    exit 1
}

usage() {
    cat <<EOF
$0 [-h] [-agnps]

DESCRIPTION

Update the system's \`hosts\` file using the StevenBlack hosts project.

The default without any flags is the unified hosts (adware + malware).

Flags may be used in combination with each other to add filters.

EXAMPLES

$0 -gnp

    Block gambling, fake news, and porn.

FLAGS

    -h
        Print help & usage.

    -a
        Block fake news, gambling, porn, and social (all).
        This is the same as using \`-gnps\`.

    -g
        Block gambling.

    -n
        Block fake news.

    -p
        Block porn.

    -s
        Block social.

EOF
}

check_root

BLOCK_FAKE_NEWS=0
BLOCK_GAMBLING=0
BLOCK_PORN=0
BLOCK_SOCIAL=0

while getopts ":hagnps" opt; do
    case ${opt} in
        h)
            usage
            exit 0
            ;;
        a)
            BLOCK_FAKE_NEWS=1
            BLOCK_GAMBLING=1
            BLOCK_PORN=1
            BLOCK_SOCIAL=1
            ;;
        g)
            BLOCK_GAMBLING=1
            ;;
        n)
            BLOCK_FAKE_NEWS=1
            ;;
        p)
            BLOCK_PORN=1
            ;;
        s)
            BLOCK_SOCIAL=1
            ;;
        \?)
            echo "Unexpected option: $OPTARG" >&2
            echo "Usage: " >&2
            usage >&2
            exit 1
            ;;
    esac
done
shift $((OPTIND - 1))

BASE_URL="https://raw.githubusercontent.com/StevenBlack/hosts"
HOSTS_URL="${BASE_URL}/master/hosts"
if [ "$BLOCK_FAKE_NEWS" -eq 1 ] \
    && [ "$BLOCK_GAMBLING" -eq 0 ] \
    && [ "$BLOCK_PORN" -eq 0 ] \
    && [ "$BLOCK_SOCIAL" -eq 0 ]; then
    HOSTS_URL="${BASE_URL}/master/alternates/fakenews/hosts"
elif [ "$BLOCK_FAKE_NEWS" -eq 0 ] \
    && [ "$BLOCK_GAMBLING" -eq 1 ] \
    && [ "$BLOCK_PORN" -eq 0 ] \
    && [ "$BLOCK_SOCIAL" -eq 0 ]; then
    HOSTS_URL="${BASE_URL}/master/alternates/gambling/hosts"
elif [ "$BLOCK_FAKE_NEWS" -eq 0 ] \
    && [ "$BLOCK_GAMBLING" -eq 0 ] \
    && [ "$BLOCK_PORN" -eq 1 ] \
    && [ "$BLOCK_SOCIAL" -eq 0 ]; then
    HOSTS_URL="${BASE_URL}/master/alternates/porn/hosts"
elif [ "$BLOCK_FAKE_NEWS" -eq 0 ] \
    && [ "$BLOCK_GAMBLING" -eq 0 ] \
    && [ "$BLOCK_PORN" -eq 0 ] \
    && [ "$BLOCK_SOCIAL" -eq 1 ]; then
    HOSTS_URL="${BASE_URL}/master/alternates/social/hosts"
elif [ "$BLOCK_FAKE_NEWS" -eq 1 ] \
    && [ "$BLOCK_GAMBLING" -eq 1 ] \
    && [ "$BLOCK_PORN" -eq 0 ] \
    && [ "$BLOCK_SOCIAL" -eq 0 ]; then
    HOSTS_URL="${BASE_URL}/master/alternates/fakenews-gambling/hosts"
elif [ "$BLOCK_FAKE_NEWS" -eq 1 ] \
    && [ "$BLOCK_GAMBLING" -eq 0 ] \
    && [ "$BLOCK_PORN" -eq 1 ] \
    && [ "$BLOCK_SOCIAL" -eq 0 ]; then
    HOSTS_URL="${BASE_URL}/master/alternates/fakenews-porn/hosts"
elif [ "$BLOCK_FAKE_NEWS" -eq 1 ] \
    && [ "$BLOCK_GAMBLING" -eq 0 ] \
    && [ "$BLOCK_PORN" -eq 0 ] \
    && [ "$BLOCK_SOCIAL" -eq 1 ]; then
    HOSTS_URL="${BASE_URL}/master/alternates/fakenews-social/hosts"
elif [ "$BLOCK_FAKE_NEWS" -eq 0 ] \
    && [ "$BLOCK_GAMBLING" -eq 1 ] \
    && [ "$BLOCK_PORN" -eq 1 ] \
    && [ "$BLOCK_SOCIAL" -eq 0 ]; then
    HOSTS_URL="${BASE_URL}/master/alternates/gambling-porn/hosts"
elif [ "$BLOCK_FAKE_NEWS" -eq 0 ] \
    && [ "$BLOCK_GAMBLING" -eq 1 ] \
    && [ "$BLOCK_PORN" -eq 0 ] \
    && [ "$BLOCK_SOCIAL" -eq 1 ]; then
    HOSTS_URL="${BASE_URL}/master/alternates/gambling-social/hosts"
elif [ "$BLOCK_FAKE_NEWS" -eq 0 ] \
    && [ "$BLOCK_GAMBLING" -eq 0 ] \
    && [ "$BLOCK_PORN" -eq 1 ] \
    && [ "$BLOCK_SOCIAL" -eq 1 ]; then
    HOSTS_URL="${BASE_URL}/master/alternates/porn-social/hosts"
elif [ "$BLOCK_FAKE_NEWS" -eq 1 ] \
    && [ "$BLOCK_GAMBLING" -eq 1 ] \
    && [ "$BLOCK_PORN" -eq 1 ] \
    && [ "$BLOCK_SOCIAL" -eq 0 ]; then
    HOSTS_URL="${BASE_URL}/master/alternates/fakenews-gambling-porn/hosts"
elif [ "$BLOCK_FAKE_NEWS" -eq 1 ] \
    && [ "$BLOCK_GAMBLING" -eq 1 ] \
    && [ "$BLOCK_PORN" -eq 0 ] \
    && [ "$BLOCK_SOCIAL" -eq 1 ]; then
    HOSTS_URL="${BASE_URL}/master/alternates/fakenews-gambling-social/hosts"
elif [ "$BLOCK_FAKE_NEWS" -eq 1 ] \
    && [ "$BLOCK_GAMBLING" -eq 0 ] \
    && [ "$BLOCK_PORN" -eq 1 ] \
    && [ "$BLOCK_SOCIAL" -eq 1 ]; then
    HOSTS_URL="${BASE_URL}/master/alternates/fakenews-porn-social/hosts"
elif [ "$BLOCK_FAKE_NEWS" -eq 0 ] \
    && [ "$BLOCK_GAMBLING" -eq 1 ] \
    && [ "$BLOCK_PORN" -eq 1 ] \
    && [ "$BLOCK_SOCIAL" -eq 1 ]; then
    HOSTS_URL="${BASE_URL}/master/alternates/gambling-porn-social/hosts"
elif [ "$BLOCK_FAKE_NEWS" -eq 1 ] \
    && [ "$BLOCK_GAMBLING" -eq 1 ] \
    && [ "$BLOCK_PORN" -eq 1 ] \
    && [ "$BLOCK_SOCIAL" -eq 1 ]; then
    HOSTS_URL="${BASE_URL}/master/alternates/fakenews-gambling-porn-social/hosts"
fi

(
    cd /tmp

    curl -O $HOSTS_URL

    hosts_path="/etc/hosts"

    first_blank_line=$(awk '/^$/{print NR}' $hosts_path | head -n 1)

    echo "Removing old hosts entries"
    sed -i $(printf '\n%s,$d' $first_blank_line) $hosts_path

    # Add a blank line to separate the entries from the existing hosts entries.
    echo "" | tee -a $hosts_path

    echo "Adding new hosts entries"
    cat /tmp/hosts | tee -a $hosts_path
)
