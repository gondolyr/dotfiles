# Set up the prompt

# NOTE: These lines break powerline
# autoload -Uz promptinit
# promptinit
# prompt adam1

setopt histignorealldups sharehistory

# Specify that ^D won't log you out.
set -o ignoreeof

# Use emacs keybindings even if our EDITOR is set to vi
# bindkey -e

# Keep 1000 lines of history within the shell and save it to ~/.zsh_history:
HISTSIZE=1000
SAVEHIST=1000
HISTFILE=~/.zsh_history

# Use modern completion system
autoload -Uz compinit
compinit

zstyle ':completion:*' auto-description 'specify: %d'
zstyle ':completion:*' completer _expand _complete _correct _approximate
zstyle ':completion:*' format 'Completing %d'
zstyle ':completion:*' group-name ''
zstyle ':completion:*' menu select=2
eval "$(dircolors -b)"
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' matcher-list '' 'm:{a-z}={A-Z}' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=* l:|=*'
zstyle ':completion:*' menu select=long
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' use-compctl false
zstyle ':completion:*' verbose true

zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#)*=0=01;31'
zstyle ':completion:*:kill:*' command 'ps -u $USER -o pid,%cpu,tty,cputime,cmd'

if [[ -x /usr/bin/dircolors ]]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.sh_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.
if [[ -r ~/.sh_aliases ]]; then
    . ~/.sh_aliases
fi

if [[ -r ~/.cargo/bin/starship ]]; then
    eval "$(starship init zsh)"
elif [[ -r /usr/share/powerline/bindings/zsh/powerline.zsh ]]; then
    . /usr/share/powerline/bindings/zsh/powerline.zsh
fi

# Colors
COLOR_NO_COLOR="[0m"
COLOR_BRIGHT="[1m"
COLOR_UNDERLINE="[4m"
# Normal
COLOR_BLACK="[0;30m"
COLOR_RED="[0;31m"
COLOR_GREEN="[0;32m"
COLOR_YELLOW="[0;33m"
COLOR_BLUE="[0;34m"
COLOR_PURPLE="[0;35m"
COLOR_CYAN="[0;36m"
COLOR_WHITE="[0;37m"
# Bright / Bold
COLOR_BRIGHT_BLACK="[1;30m"
COLOR_BRIGHT_RED="[1;31m"
COLOR_BRIGHT_GREEN="[1;32m"
COLOR_BRIGHT_YELLOW="[1;33m"
COLOR_BRIGHT_BLUE="[1;34m"
COLOR_BRIGHT_PURPLE="[1;35m"
COLOR_BRIGHT_CYAN="[1;36m"
COLOR_BRIGHT_WHITE="[1;37m"
# Bright text with background color
COLOR_BRIGHT_YELLOW_BRIGHT_BLUE_BG="[1;44;33m"

# Colored `man` pages
man() {
    env \
        LESS_TERMCAP_mb=$(printf "\e$COLOR_BRIGHT_RED") \
        LESS_TERMCAP_md=$(printf "\e$COLOR_BRIGHT_RED") \
        LESS_TERMCAP_me=$(printf "\e$COLOR_NO_COLOR") \
        LESS_TERMCAP_se=$(printf "\e$COLOR_NO_COLOR") \
        LESS_TERMCAP_so=$(printf "\e$COLOR_BRIGHT_YELLOW_BRIGHT_BLUE_BG") \
        LESS_TERMCAP_ue=$(printf "\e$COLOR_NO_COLOR") \
        LESS_TERMCAP_us=$(printf "\e$COLOR_BRIGHT_GREEN") \
            man "$@"
}

# Fuzzy finder
# See: https://github.com/junegunn/fzf
if [[ -r /usr/share/doc/fzf/examples/key-bindings.zsh ]]; then
    source /usr/share/doc/fzf/examples/key-bindings.zsh
fi

fpath+=${ZDOTDIR:-~}/.zsh_functions
