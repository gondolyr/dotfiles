<!--
SPDX-FileCopyrightText: 2024 gondolyr <gondolyr+code@posteo.org>

SPDX-License-Identifier: CC0-1.0
-->

# dotfiles

A public collection of my dotfile configurations.

## /etc

This directory contains configuration files for the `/etc/` directory.

| File                                          | Description                                                                                                                                                                                                                              |
|-----------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `etc/modprobe.d/audio_disable_powersave.conf` | Disables audio power saving so that there is no audible popping when the sound card is turned on/off. See: <https://wiki.archlinux.org/title/Advanced_Linux_Sound_Architecture/Troubleshooting#Pops_when_starting_and_stopping_playback> | 

## Other

| Directory/File | Description                     | Install Location |
|----------------|---------------------------------|------------------|
| `.gitconfig`   | Global Git configuration        | `$HOME/`         |

# License

<p xmlns:cc="http://creativecommons.org/ns#" >This work is marked with <a href="https://creativecommons.org/publicdomain/zero/1.0/" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC0 1.0<img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg" alt=""><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/zero.svg" alt=""></a> unless stated otherwise.</p>

The files under the `scripts/` directory are licensed under the [GPLv3+](https://www.gnu.org/licenses/gpl-3.0.html).
