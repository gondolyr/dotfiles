# dotfiles

A public collection of my dotfile configurations.

# Configuration

- `$XDG_CONFIG_HOME` defines the base directory relative to which user-specific configuration files should be stored.
  If `$XDG_CONFIG_HOME` is either not set or empty, a default equal to `$HOME/.config` should be used. \[[Source](https://specifications.freedesktop.org/basedir-spec/latest/ar01s03.html)\]
- On Windows, `%APPDATA%` generally points to `C:\Users\$USER\AppData\Roaming`.

## .config

This section describes the configuration files under the `.config/` directory.

| Program   | Supported Platforms     | Install Location                                                                                                                                     |
|-----------|-------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------|
| Alacritty | Linux<br>Mac<br>Windows | Linux and Mac: `$XDG_CONFIG_HOME/.config/alacritty`<br>Windows: `%APPDATA%\alacritty`                                                                |
| Helix     | Linux<br>Mac<br>Windows | Linux and Mac: `$XDG_CONFIG_HOME/.config/helix`<br>Windows: `%APPDATA%\helix`                                                                        |
| MPV       | Linux<br>Mac<br>Windows | Linux (Flatpak) `$HOME/.var/app/io.mpv.Mpv/config/mpv`<br>Linux (Non-Flatpak) and Mac: `$HOME/.config/mpv`<br>Windows: `%APPDATA%\mpv`               |
| nixpkgs   | LInux<br>Mac            | Linux and Mac: `$XDG_CONFIG_HOME/.config/nixpkgs`                                                                                                    |
| Nushell   | Linux<br>Mac<br>Windows | Linux and Mac: `$XDG_CONFIG_HOME/.config/nushell`<br>Windows: `%APPDATA%\nushell`                                                                    |
| wezterm   | Linux<br>Mac<br>Windows | Linux and Mac: `$XDG_CONFIG_HOME/wezterm`<br>Linux (Flatpak): `$HOME/.var/app/org.wezfurlong.wezterm/config/wezterm`<br>Windows: `%APPDATA%\wezterm` |
| yt-dlp    | Linux<br>Mac<br>Windows | Linux and Mac: `$XDG_CONFIG_HOME/.config/yt-dlp`<br>Windows: `%APPDATA%\yt-dlp`                                                                      |
| zellij    | Linux<br>Mac<br>Windows | Linux and Mac: `$XDG_CONFIG_HOME/.config/zellij`<br>Windows: `%APPDATA%\zellij`                                                                      |

## etc

This directory contains configuration files for the `/etc/` directory.

| File                                          | Description                                                                                                                                                                                                                              |
|-----------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `etc/modprobe.d/audio_disable_powersave.conf` | Disables audio power saving so that there is no audible popping when the sound card is turned on/off. See: <https://wiki.archlinux.org/title/Advanced_Linux_Sound_Architecture/Troubleshooting#Pops_when_starting_and_stopping_playback> | 

## Other

| Directory/File | Description                     | Install Location |
|----------------|---------------------------------|------------------|
| `.gitconfig`   | Global Git configuration        | `$HOME/`         |
| `.gitignore`   | Global Git ignore configuration | `$HOME/`         |
| `.vimrc`       | Vim configuration               | `$HOME/`         |
| `nixos`        | NixOS configuration             | `/etc/nixos`     |

# Helix

## Installation

1. Choose one of the installation methods described on their [Installation page](https://docs.helix-editor.com/install.html).
2. Copy the `runtime/` directory into the config directory as described here: <https://docs.helix-editor.com/install.html#build-from-source>.
   This will enable things such as syntax highlighting and the default themes.
3. [Build the tree-sitter grammars](https://docs.helix-editor.com/install.html#building-tree-sitter-grammars) to finalize the language grammar installation, which will enable the syntax highlighting.
    ```sh
    hx --grammar fetch
    hx --grammar build
    ```

# nixpkgs

## Installation

If using the [Nix package manager](https://nixos.org/), first place the nixpkgs config in the appropriate folder (details in the [.config section](#config)).
Once the configs are installed, run the following to install a package group:

```sh
nix-env -iA nixpkgs.<package-group>
```

The list of package groups can be found below in the [Package Groups section](#package-groups).

## Package Groups

| Group Name               | Description                                                |
|--------------------------|------------------------------------------------------------|
| `all-packages`           | Convenience group to install all packages from all groups. |
| `app-packages`           | General applications.                                      |
| `development-packages`   | Packages used for software development.                    |
| `digital-media-packages` | Packages used for digital media production.                |
| `tools-packages`         | Specialized tools for things like batch downloading.       |

## Upgrading Using Channels

<https://nixos.org/manual/nix/stable/package-management/channels.html>

Upgrade all packages in the Nix profile to the latest versions available with:

```sh
nix-channel --update
nix-env --upgrade
```

## Uninstalling a Nix Package

```sh
nix-env --uninstall <package-name>
```

## Uninstalling Nix

The following instructions are from the Nix manual (<https://nixos.org/manual/nix/stable/installation/installing-binary.html#uninstalling>).

```sh
sudo rm -rf /etc/profile/nix.sh /etc/nix /nix ~root/.nix-profile ~root/.nix-defexpr ~root/.nix-channels ~/.nix-profile ~/.nix-defexpr ~/.nix-channels

# If you are on Linux with systemd, you will need to run:
sudo systemctl stop nix-daemon.socket
sudo systemctl stop nix-daemon.service
sudo systemctl disable nix-daemon.socket
sudo systemctl disable nix-daemon.service
sudo systemctl daemon-reload
```

There may also be references to Nix in `/etc/profile`, `/etc/bashrc`, and `/etc/zshrc` which you may remove.

# Software I Use

| Name                                                        | Description                                                                                           | Preview                                                                                                                       |
|-------------------------------------------------------------|-------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------|
| [Alacritty](https://github.com/alacritty/alacritty)         | A cross-platform, OpenGL terminal emulator.                                                           | ![alacritty screenshot](https://user-images.githubusercontent.com/8886672/103264352-5ab0d500-49a2-11eb-8961-02f7da66c855.png) |
| [bat](https://github.com/sharkdp/bat)                       | A cat(1) clone with wings.                                                                            | ![bat header logo image](https://raw.githubusercontent.com/sharkdp/bat/master/doc/logo-header.svg)                            |
| [cargo-outdated](https://github.com/kbknapp/cargo-outdated) | A cargo subcommand for displaying when Rust dependencies are out of date.                             | N/A                                                                                                                           |
| [exa](https://github.com/ogham/exa)                         | A modern replacement for ‘ls’.                                                                        | ![exa screenshot](https://raw.githubusercontent.com/ogham/exa/master/screenshots.png)                                         |
| [git-delta](https://github.com/dandavison/delta)            | A syntax-highlighting pager for git, diff, and grep output.                                           | ![git-delta screenshot](https://user-images.githubusercontent.com/52205/86275526-76792100-bba1-11ea-9e78-6be9baa80b29.png)    |
| [helix](https://github.com/helix-editor/helix)              | A post-modern modal text editor.                                                                      | ![helix screenshot](https://raw.githubusercontent.com/helix-editor/helix/master/screenshot.png)                               |
| [jless](https://github.com/PaulJuliusMartinez/jless)        | jless is a command-line JSON viewer designed for reading, exploring, and searching through JSON data. | ![jless screenshot](https://jless.io/assets/jless-recording.gif)                                                              |
| [mpv](https://github.com/mpv-player/mpv)                    | Command line video player.                                                                            | ![mpv screenshot](https://mpv.io/images/mpv-screenshot-34cd36ae.jpg)                                                          |
| [Nushell](https://github.com/nushell/nushell)               | A new type of shell.                                                                                  | ![nushell screenshot](https://raw.githubusercontent.com/nushell/nushell/main/images/nushell-autocomplete6.gif)                |
| [ripgrep](https://github.com/BurntSushi/ripgrep)            | ripgrep recursively searches directories for a regex pattern while respecting your gitignore.         | ![ripgrep screenshot](https://burntsushi.net/stuff/ripgrep1.png)                                                              |
| [starship](https://github.com/starship/starship)            | The minimal, blazing-fast, and infinitely customizable prompt for any shell!                          | ![starship screenshot](https://raw.githubusercontent.com/starship/starship/master/media/demo.gif)                             |
| [twitch-dl](https://github.com/ihabunek/twitch-dl)          | CLI tool for downloading videos from Twitch.                                                          | N/A                                                                                                                           |
| [yt-dlp](https://github.com/yt-dlp/yt-dlp)                  | A youtube-dl fork with additional features and fixes.                                                 | ![yt-dlp header logo](https://raw.githubusercontent.com/yt-dlp/yt-dlp/master/.github/banner.svg)                              |
| [zellij](https://github.com/zellij-org/zellij)              | A terminal workspace with batteries included.                                                         | ![zellij screenshot](https://raw.githubusercontent.com/zellij-org/zellij/main/assets/demo.gif)                                |
