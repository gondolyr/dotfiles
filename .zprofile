if [ -z "${DISPLAY+x}" ] && [ -n "$XDG_VTNR" ] && [ "$XDG_VTNR" -eq 1 ]; then
    exec startx
fi

# Start or attach to a tmux session called "base" on logins via SSH.
if [ -z "${TMUX+x}" ] && [ -n "$SSH_CONNECTION" ]; then
    if ! tmux has-session -t "base" 2> /dev/null; then
        tmux new-session -ds "base"
    fi
    exec tmux attach-session -t "base" > /dev/null
fi
