.PHONY: all install update install-git-configs update-git-configs install-git-templates update-git-templates install-shell-configs update-shell-configs install-vim-configs update-vim-configs install-youtube-dl-config update-youtube-dl-config install-mpv-config update-mpv-config install-alacritty-config update-alacritty-config install-nu-config update-nu-config

all: install

install: install-git-configs install-git-templates install-vim-configs install-shell-configs install-alacritty-config install-nu-config

update: update-git-configs update-git-templates update-vim-configs update-shell-configs update-alacritty-config

install-alacritty-config:
	cp -rf .config/alacritty ~/.config/

update-alacritty-config:
	cp -a ~/.config/alacritty .config/

install-git-configs:
	cp .gitconfig ~/
	cp .gitignore ~/

update-git-configs:
	cp ~/.gitconfig .
	cp ~/.gitignore .

install-git-templates:
	cp -rf .git-templates ~/

update-git-templates:
	cp -a ~/.git-templates .

install-mpv-configs:
	cp -rf .config/mpv ~/.config/

update-mpv-configs:
	cp -a ~/.config/mpv .config/

install-nu-config:
	cp -rf .config/nushell ~/.config/

update-nu-config:
	cp -a ~/.config/nushell .config/

install-shell-configs:
	cp .sh_aliases ~/
	cp .zshrc ~/
	cp .zprofile ~/
	cp .zshenv ~/
	cp -rf .zsh_functions ~/
	source ~/.zshrc

update-shell-configs:
	cp ~/.sh_aliases .
	cp ~/.zshrc .
	cp ~/.zprofile .
	cp ~/.zshenv .

install-vim-configs:
	cp .vimrc ~/

update-vim-configs:
	cp ~/.vimrc .

install-youtube-dl-config:
	cp -rf .config/yt-dlp ~/.config/

update-youtube-dl-config:
	cp -a ~/.config/yt-dlp .config/
