-- SPDX-FileCopyrightText: 2024 gondolyr <gondolyr+code@posteo.org>
--
-- SPDX-License-Identifier: CC0-1.0

-- Pull in the wezterm API
local wezterm = require 'wezterm'
local mux = wezterm.mux

-- This table will hold the configuration.
local config = {}

-- In newer versions of wezterm, use the config_builder which will
-- help provide clearer error messages
if wezterm.config_builder then
  config = wezterm.config_builder()
end

wezterm.on('gui-startup', function(cmd)
  -- allow `wezterm start -- something` to affect what we spawn
  -- in our initial window
  local args = {}
  if cmd then
    args = cmd.args
  end

  local tab, pane, window = mux.spawn_window { }
  window:gui_window():maximize()
end)

wezterm.on('format-window-title', function(tab, pane, tabs, panes, config)
  local zoomed = ''
  if tab.active_pane.is_zoomed then
    zoomed = '[Z] '
  end

  local index = ''
  if #tabs > 1 then
    index = string.format('[%d/%d] ', tab.tab_index + 1, #tabs)
  end

  -- return zoomed .. index .. tab.active_pane.title

  return zoomed .. index .. 'WezTerm'
end)

-- This is where you actually apply your config choices

config.automatically_reload_config = true
config.default_prog = { 'zellij', 'attach', '-c', 'main' }
config.enable_tab_bar = false
config.font = wezterm.font { family = 'Fira Code', weight = 'Bold' }
config.front_end = 'WebGpu'
config.window_close_confirmation = 'NeverPrompt'
config.window_decorations = 'RESIZE'

config.color_schemes = {
  ['Alacritty'] = {
    foreground = '#c5c9c6',
    background = '#1d1f21',

    -- cursor_bg = '',

    ansi = {
      '#1d1f21', -- black
      '#cc6666', -- red
      '#b5bd68', -- green
      '#f0c674', -- yellow
      '#81a2be', -- blue
      '#b294bb', -- magenta
      '#8abeb7', -- cyan
      '#c5c8c6', -- white
    },

    brights = {
      '#666666', -- black
      '#d54e53', -- red
      '#b9ca4a', -- green
      '#e7c547', -- yellow
      '#7aa6da', -- blue
      '#c397d8', -- magenta
      '#70c0b1', -- cyan
      '#eaeaea', -- white
    },
  },
  ['Seti (Soft)'] = {
    foreground = '#c5c9c6',
    background = '#1d1f21',

    cursor_fg = '#1d1f21',
    cursor_bg = '#c5c9c6',

    -- selection_fg = '#1d1f21',
    -- selection_bg = '#c5c9c6',

    ansi = {
      '#333333', -- black
      '#cc0000', -- red
      '#4e9a06', -- green
      '#c4a000', -- yellow
      '#3465a4', -- blue
      '#75507b', -- magenta
      '#06989a', -- cyan
      '#d3d7cf', -- white
    },

    brights = {
      '#88807c', -- black
      '#f15d22', -- red
      '#73c48f', -- green
      '#ffce51', -- yellow
      '#48b9c7', -- blue
      '#ad7fa8', -- magenta
      '#34e2e2', -- cyan
      '#eeeeec', -- white
    },
  },
}

-- For example, changing the color scheme:
-- config.color_scheme = 'AdventureTime'
-- config.color_scheme = 'Seti'
-- config.color_scheme = 'Alacritty'
config.color_scheme = 'Seti (Soft)'

-- and finally, return the configuration to wezterm
return config
