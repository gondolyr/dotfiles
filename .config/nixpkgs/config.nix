{
  packageOverrides = pkgs: with pkgs; {
    all-packages = pkgs.buildEnv {
      name = "all-packagess";
      paths = [
        app-packages
        development-packages
        digital-media-packages
        tools-packages
      ];
    };
    app-packages = pkgs.buildEnv {
      name = "app-packages";
      paths = [
        # Accounting
        gnucash      # Free software for double entry accounting
        skrooge      # A personal finances manager, powered by KDE

        # BitTorrent
        qbittorrent  # Featureful free software BitTorrent client

        # Media
        gpodder      # A podcatcher written in python
        mpv          # General-purpose media player, fork of MPlayer and mplayer2

        # Productivity
        libreoffice  # Comprehensive, professional-quality productivity suite, a variant of openoffice.org
        
        # Web Browser
        #firefox      # A web browser built from Firefox source tree
        librewolf    # A fork of Firefox, focused on privacy, security and freedom
      ];
    };
    development-packages = pkgs.buildEnv {
      name = "development-packages";
      paths = [
        alacritty       # A cross-platform, GPU-accelerated terminal emulator
        bat             # A cat(1) clone with syntax highlighting and Git integration
        cargo-outdated  # A cargo subcommand for displaying when Rust dependencies are out of date
        cargo-watch     # A Cargo subcommand for watching over Cargo project's source
        delta           # A syntax-highlighting pager for git
        exa             # Replacement for 'ls' written in Rust
        jless           # A command-line pager for JSON data
        helix           # A post-modern modal text editor
        lapce           # Lightning-fast and Powerful Code Editor written in Rust
        nushell         # A modern shell written in Rust
        ripgrep         # A utility that combines the usability of The Silver Searcher with the raw speed of grep
        starship        # A minimal, blazing fast, and extremely customizable prompt for any shell
        zellij          # A terminal workspace with batteries included
      ];
    };
    digital-media-packages = pkgs.buildEnv {
      name = "digital-media-packages";
      paths = [
        ardour      # Multi-track hard disk recording software
        blender     # 3D Creation/Animation/Publishing System
        darktable   # Virtual lighttable and darkroom for photographers
        gimp        # The GNU Image Manipulation Program
        inkscape    # Vector graphics editor
        obs-studio  # Free and open source software for video recording and live streaming
      ];
    };
    tools-packages = pkgs.buildEnv {
      name = "tools-packages";
      paths = [ 
        barrier  # Open-source KVM software
        yt-dlp   # Command-line tool to download videos from YouTube.com and other sites (youtube-dl fork)
      ];
    };
  };
}
