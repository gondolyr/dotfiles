# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running `nixos-help`).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  # Use the extlinux boot loader. (NixOS wants to enable GRUB by default)
  boot.loader.grub.enable = false;
  # Enables the generation of /boot/extlinux/extlinux.conf
  boot.loader.generic-extlinux-compatible.enable = true;

  # networking.hostName = "nixos"; # Define your hostname.
  # Pick only one of the below networking options.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.
  # networking.networkmanager.enable = true;  # Easiest to use and most distros use this by default.

  # Assign a static IP address instead of using DHCP to make the server more stable.
  networking.interfaces.end0.ipv4.addresses = [ {
    address = "192.168.1.118";
    prefixLength = 24;
  } ];
  networking.defaultGateway = "192.168.1.1";
  networking.nameservers = [ "8.8.8.8" ];

  # Set your time zone.
  # time.timeZone = "Europe/Amsterdam";

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
  # i18n.defaultLocale = "en_US.UTF-8";
  # console = {
  #   font = "Lat2-Terminus16";
  #   keyMap = "us";
  #   useXkbConfig = true; # use xkbOptions in tty.
  # };

  # Enable the X11 windowing system.
  # services.xserver.enable = true;

  # Configure keymap in X11
  # services.xserver.layout = "us";
  # services.xserver.xkbOptions = "eurosign:e,caps:escape";

  # Enable CUPS to print documents.
  # services.printing.enable = true;

  # Enable sound.
  # sound.enable = true;
  # hardware.pulseaudio.enable = true;

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  # users.users.alice = {
  #   isNormalUser = true;
  #   extraGroups = [ "wheel" ]; # Enable ‘sudo’ for the user.
  #   packages = with pkgs; [
  #     firefox
  #     tree
  #   ];
  # };
  users = {
    # Disable useradd and passwd.
    #mutableUsers = false;

    users = {
      root = {
        # Disable root password.
        hashedPassword = "*";
      };
      # This user will not have a password set and will need one (with passwd) to be able to SSH as them.
      styx = {
        isNormalUser = true;
        extraGroups = [ "wheel" ]; # Enable 'sudo' for the user.
      };
      # Create service users.
      conduit = {
        isSystemUser = true;
        group = "conduit";
      };
    };

    groups = {
      conduit = {};
    };
  };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    caddy
    helix
    matrix-conduit
    vim # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.
    wget
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh = {
    enable = true;
    # Require public key authentication for better security.
    settings.PasswordAuthentication = false;
    settings.KbdInteractiveAuthentication = false;
    #settings.PermitRootLogin = "yes";
  };

  systemd.services.matrix-conduit = {
    serviceConfig = {
      Type = "simple";
      User = "conduit";
      Group = "conduit";
    };
  };
  services.matrix-conduit = {
    enable = true;
    settings.global = {
      # The server_name is the pretty name of this server. It is used as a suffix for user
      # and room ids. Examples: matrix.org, conduit.rs

      # The Conduit server needs all /_matrix/ requests to be reachable at
      # https://your.server.name/ on port 443 (client-server) and 8448 (federation).

      # If that's not possible for you, you can create /.well-known files to redirect
      # requests. See
      # https://matrix.org/docs/spec/client_server/latest#get-well-known-matrix-client
      # and
      # https://matrix.org/docs/spec/server_server/r0.1.4#get-well-known-matrix-server
      # for more information

      # YOU NEED TO EDIT THIS
      # The server_name is the name of this server. It is used as a suffix for user # and room ids.
      #server_name = "your.server.name";

      # This is the only directory where Conduit will save its data
      # database_path can not be edited because the service's reliance on systemd StateDir.
      #database_path = "/var/lib/matrix-conduit/";
      database_backend = "rocksdb";

      # The port Conduit will be running on. You need to set up a reverse proxy in
      # your web server (e.g. apache or nginx), so all requests to /_matrix on port
      # 443 and 8448 will be forwarded to the Conduit instance running on this port
      # Docker users: Don't change this, you'll need to map an external port to this.
      port = 6167;

      # Max size for uploads. Don't forget to also change it in the proxy.
      max_request_size = 20000000; # in bytes, 20 MB

      # Enables registration. If set to false, no users can register on this server.
      allow_registration = true;

      # Whether this server federates with other servers.
      allow_federation = true;
      #allow_check_for_updates = true;
      # Whether new encrypted rooms can be created. Note: existing rooms will continue to work.
      #allow_encryption = true;

      # Server to get public keys from. You probably shouldn't change this
      trusted_servers = ["matrix.org"];

      #max_concurrent_requests = 100 # How many requests Conduit sends to other servers at the same time
      #log = "warn,state_res=warn,rocket=off,_=off,sled=off";

      # Address to listen on for connections by the reverse proxy/tls terminator.
      address = "127.0.0.1"; # This makes sure Conduit can only be reached using the reverse proxy
      #address = "0.0.0.0"; # If Conduit is running in a container, make sure the reverse proxy (ie. Traefik) can reach it.
    };
  };

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # Copy the NixOS configuration file and link it from the resulting system
  # (/run/current-system/configuration.nix). This is useful in case you
  # accidentally delete configuration.nix.
  # system.copySystemConfiguration = true;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It's perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.05"; # Did you read the comment?

}

